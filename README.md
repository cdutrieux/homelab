
## Infrastructure

These two machines are connected together via a wireguard VPN tunnel.

 - OVH vps: 10.8.0.1
 - home server: 10.8.0.2

## Services

### on VPS

 - Prometheus


### on home server

 - Cadvisor
 - Watchtower


#### Cadvisor 

https://samber.github.io/awesome-prometheus-alerts/rules#rule-docker-containers-1-2
