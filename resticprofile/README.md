Configuration to backup the homelab

 - Create backup
 - Backup status added to prometheus monitoring


To set up, it's needed:
 - to create a file `cdutrieux-selfhost-backups.key` containing the
   client side encryption key
 - to create a systemd service with
  ```
  resticprofile -c profiles.yaml schedule --all
  ```
 - to edit the systemd service and add env variables containing
   credentials for the bucket. Today, editing the service creates a
   file similar to `systemd-override.conf`.


Also see:
https://helgeklein.com/blog/restic-encrypted-offsite-backup-with-ransomware-protection-for-your-homeserver/
